'Purpose: Hides rows for different views on different sheets.  Hides, renames, and deletes rows for specific MFG sheet conditions
'Author: Christopher Kajioka

Sub HideRows()
    'Checking to see if it should run code against sheet names listed in AlwaysCallFirst Module
    'Start of hide rows code
    Dim rangedata As Range
    Dim rgToDelete As Range
    Dim rgToFit As Range
    Dim r As Range
    Dim i As Long
    
    'Hide QC Rows
    If ActiveSheet.Name = QC_Sheet_Name Then
        Set rangedata = Range(StartPoint).CurrentRegion
        rangedata.AutoFilter Field:=TestTypeColNumber, Criteria1:=QC_stringtofilter, Operator:=xlOr, Criteria2:="*N/A*"       'Added N/A Test Type to QC View as well
    End If
    
    'Hide TD Rows
    If ActiveSheet.Name = TD_Sheet_Name Then
        Set rangedata = Range(StartPoint).CurrentRegion
        rangedata.AutoFilter Field:=TestTypeColNumber, Criteria1:=TD_stringtofilter
    End If
    
    'Hide EM Rows
    If ActiveSheet.Name = EM_Sheet_Name Then
        Set rangedata = Range(StartPoint).CurrentRegion
        rangedata.AutoFilter Field:=TestTypeColNumber, Criteria1:=EM_stringtofilter
    End If
    
    'Hide MS Rows
    If ActiveSheet.Name = MS_Sheet_Name Then
        Set rangedata = Range(StartPoint).CurrentRegion
        rangedata.AutoFilter Field:=TestTypeColNumber, Criteria1:=MS_stringtofilter
    End If
    
    'Hide Dispo Rows
    If ActiveSheet.Name = Dispo_Sheet_Name Then
        Set rangedata = Range(StartPoint).CurrentRegion
        rangedata.AutoFilter Field:=BatchReleaseColNumber, Criteria1:=Dispo_stringtofilter
    End If

    'Hide MFG View Row Names
    If InStr(MFGViewNames, ";" & ActiveSheet.Name) <> 0 Then
        
         'Hide rows not needed for sheet, added 16Oct2018
        Set rangedata = Range(StartPoint).CurrentRegion
        rangedata.AutoFilter Field:=BPRColNumber, Criteria1:=ActiveSheet.Name
                
        'Search for merged cells and not QC_LIMS and not MFG to aliquot samples, replace text with Multiple Tests
        For i = rowtofilter To MasterLastRow
            If Rows(i).EntireRow.Hidden = False And Range(SLColLetter & i).MergeArea.Cells.Count > 1 And Range(TestTypeColLetter & i) <> MFG_stringtofilter1 And Range(AliqGroupColLetter & i) <> MFG_stringtofilter2 Then
                Range(TestNameColLetter & i).Value = "Multiple Tests"
                Range(AliqQtyColLetter & i).Value = "N/A"
                Range(AliqVolColLetter & i).Value = "N/A"
                Range(AliqContColLetter & i).Value = "N/A"

                If rgToFit Is Nothing Then
                    Set rgToFit = Range(SLColLetter & i)
                Else
                    Set rgToFit = Union(rgToFit, Range(SLColLetter & i))
                End If
            End If
            
            If Rows(i).EntireRow.Hidden = False And Range(SLColLetter & i).Value = 0 And Range(TestTypeColLetter & i) <> MFG_stringtofilter1 And Range(AliqGroupColLetter & i) <> MFG_stringtofilter2 Then
                If rgToDelete Is Nothing Then
                    Set rgToDelete = Range(SLColLetter & i)
                Else
                    Set rgToDelete = Union(rgToDelete, Range(SLColLetter & i))
                End If
            End If
        Next i
        
        'To remove autofilter, prevent unmerging cells
        On Error Resume Next
        ActiveSheet.ShowAllData
        On Error GoTo 0

        If Not rgToDelete Is Nothing Then
            rgToDelete.EntireRow.Delete
        End If
        
        'Addition so single row will fit in mfg views when rows are deleted
        If Not rgToFit Is Nothing Then
        
            rgToFit.EntireRow.AutoFit
            
            For Each r In rgToFit.Rows
                If r.EntireRow.RowHeight < MinRowHeight Then
                    r.EntireRow.RowHeight = MinRowHeight
                End If
            Next

        End If
        
        'Reset autofilter
        rangedata.AutoFilter Field:=BPRColNumber, Criteria1:=ActiveSheet.Name
    End If
End Sub
