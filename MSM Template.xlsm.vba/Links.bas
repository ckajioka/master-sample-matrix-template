'Purpose: Link to various sheets from Start Here sheet
'Author: Christopher Kajioka

Sub StartHereLink()
    Dim linkws As Worksheet
    Set linkws = ThisWorkbook.Sheets("Start Here")
        
    ThisWorkbook.ActiveSheet.Visible = False
    
    If linkws.Visible = False Then
        linkws.Visible = True
    End If
    
    linkws.Activate
End Sub

Sub TroubleshootingLink()
    Dim linkws As Worksheet
    Set linkws = ThisWorkbook.Sheets("Troubleshooting")
    
    If linkws.Visible = False Then
        linkws.Visible = True
    End If
    
    linkws.Activate
End Sub

Sub GuideLink()
    Dim linkws As Worksheet
    Set linkws = ThisWorkbook.Sheets("Guide")
    
    If linkws.Visible = False Then
        linkws.Visible = True
    End If

    linkws.Activate
End Sub

Sub SummaryLink()
    Dim linkws As Worksheet
    Set linkws = ThisWorkbook.Sheets("Macro Summary")
    
    If linkws.Visible = False Then
        linkws.Visible = True
    End If

    linkws.Activate
End Sub