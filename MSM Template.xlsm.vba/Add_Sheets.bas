'Purpose: Add sheets to project
'Author: Christopher Kajioka

'Adds Group and MFG view sheets
Sub AddSheets()
    Dim ws As Worksheet
    Dim wb As Workbook
    Dim starting_ws1 As Worksheet
    Dim exists As Boolean
    Dim i As Long
    
    Set ws = ActiveSheet
    Set wb = ActiveWorkbook
    Dim d As Range
    
    'Sets start page when complete macro is not being run
    If MacroValue = False Then
        Set starting_ws1 = ActiveSheet
    End If
    '
    'Adds sheets.  Note: Could maybe make the range dynamic here (kind of like what is used in the CopyToString module)
    For Each d In Sheets("Lists").Range("AA6:AA32")
        If d <> "" Then
            exists = False
            For i = 1 To Worksheets.Count
                If Worksheets(i).Name = d Then
                    exists = True
                    Exit For 'new to speed up
                End If
            Next i
                
            If Not exists Then
                With wb
                    .Sheets.Add After:=.Sheets(.Sheets.Count)
                    On Error Resume Next
                    ActiveSheet.Name = d.Value
                    On Error GoTo 0
                End With
            End If
        End If
    Next d
       
    'Brings view back to first starting worksheet
    If MacroValue = False Then
        starting_ws1.Activate
    End If
End Sub