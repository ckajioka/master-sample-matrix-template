'Purpose: Hides row filter arrows from sheet, please run before the HideRows module
'Author: Christopher Kajioka

Sub HideFilters()
    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    If InStr(CopySheetNames, ";" & ws.Name) <> 0 Then

        'Start of hide filters code
        Dim c As Range
        Dim i As Long

        'Check for filter, turn on if none exists
        If Not ActiveSheet.AutoFilterMode Then
            ActiveSheet.Range(StartPoint).AutoFilter
        End If
        
        'This removes filter arrows
        i = 0
        For Each c In ActiveSheet.AutoFilter.Range.Rows(rowtofilter).Cells
            i = i + 1
            If i > 0 Then
                c.AutoFilter Field:=i, Visibledropdown:=False
            End If
        Next c

    End If
End Sub
