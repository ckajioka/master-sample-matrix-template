'Purpose: Copy from Master View sheet, adds report/attachment information to sheets, and fills batch ID into each MFG sheet
'Author: Christopher Kajioka

'Copy from Master View sheet
Sub CopyMaster()
    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    Dim source As Worksheet
    Dim Target As Worksheet
    
    If InStr(CopySheetNames, ";" & ws.Name) <> 0 Then
    
        'Start of Copy Module
        Set source = ThisWorkbook.Worksheets(MasterView)
        Set Target = ThisWorkbook.ActiveSheet
        
        source.Cells.Copy
        
        With Target.Cells
'            .PasteSpecial Paste:=xlPasteColumnWidths
'            .PasteSpecial Paste:=xlPasteFormats
'            .PasteSpecial Paste:=xlPasteFormulasAndNumberFormats
            
            'Added the this paste special below to keep partially highlighted cells, only downside is that it copies validation as well
            .PasteSpecial Paste:=xlPasteAllUsingSourceTheme
            
            Application.CutCopyMode = False
        End With
        
    End If
      
    If InStr(MFGViewNames, ";" & ws.Name) <> 0 Then
        ws.PageSetup.RightFooter = _
                                             "&12&B&""Arial""MFG Initial/Date ____________________     QA Initial/Date ____________________&B&""Arial""&12&B&""Arial"""
    End If
    
'    If ws.Name = StartHereBPRName Then
'        ws.PageSetup.RightFooter = _
'                                             "&12&B&""Arial""Fill Site ID Aliquots Initial/Date ____________________     MFG Initial/Date ____________________     QA Initial/Date ____________________&B&""Arial""&12&B&""Arial"""
'    End If
    
End Sub


'Fills report and attachment information to sheet
Sub AddAttachName()
    Dim i As Long
    Dim AttachNumber As Long
    Dim cell As Range
    i = 9
    Do While Columns(i).EntireColumn.Hidden = True
        i = i + 1
    Loop
        
    For Each cell In CSNRange.Cells
        If cell.Value = ActiveSheet.Name Then
            AttachNumber = cell.Offset(0, 1).Value
            Exit For
        End If
    Next cell
    
    'If shipping view is disabled then subtract 1 from attachment number.
    If ShippingViewOption = False Then
        AttachNumber = AttachNumber - 1
    End If
    
    'Remove attachment number from copied sheets before adding new values
    Cells(1, TestNameColNumber).Value = ""
    Cells(2, TestNameColNumber).Value = ""
        
    Cells(rowtofilter - 2, i).Value = "Attachment " & AttachNumber & ": " & ActiveSheet.Name
    Cells(rowtofilter - 2, i).Font.Bold = True
    Cells(rowtofilter - 2, i).Font.Size = 18
    Cells(rowtofilter - 2, i).Font.Name = Arial
            
    Cells(rowtofilter - 3, i).Value = Sheets("Start Here").Range("E2").Value
    Cells(rowtofilter - 3, i).Font.Bold = True
    Cells(rowtofilter - 3, i).Font.Size = 18
    Cells(rowtofilter - 3, i).Font.Name = Arial
End Sub

'Fills batch ID to each MFG View
Sub AddBatchID()
    Dim i As Long
    i = MasterLastCol - 2
    
    Do While Columns(i).EntireColumn.Hidden = True
        i = i - 1
    Loop
        
    Cells(rowtofilter - 2, i).Value = "BATCH ID:________________"
    Cells(rowtofilter - 2, i).Font.Bold = True
    Cells(rowtofilter - 2, i).Font.Size = 18
    Cells(rowtofilter - 2, i).Font.Name = Arial
End Sub

'Adds a border to the right side of the EM View
Sub AddEMBorder()
    Dim i As Long
    Dim iColLetter As String
    i = MasterLastCol + 1
    iColLetter = Split(Cells(1, i).Address, "$")(1)
    
    For i = rowtofilter To MasterLastRow
        If Rows(i).EntireRow.Hidden = False Then
            Range(iColLetter & i).Borders(xlEdgeLeft).LineStyle = xlContinuous
        End If
    Next i
End Sub

'Copies from template sheet to Master View
Sub CopyTemplate()
    Dim source As Worksheet
    Dim Target As Worksheet
    Set source = ThisWorkbook.Worksheets(ListName)
    Set Target = ThisWorkbook.Worksheets(MasterView)
        
    source.Cells.Copy
        
    With Target.Cells
        .PasteSpecial Paste:=xlPasteColumnWidths
        .PasteSpecial Paste:=xlPasteFormats
        .PasteSpecial Paste:=xlPasteFormulasAndNumberFormats
        Application.CutCopyMode = False
    End With
End Sub

'Add sheet names to report page
Sub ReportAddAttach()
    Dim i As Long
    Dim AttachNumber As Long
    Dim cell As Range
    
    ThisWorkbook.Sheets(ReportView).Range("B43:B76").ClearContents
    
    If ShippingViewOption = False Then
        i = 43
        For Each cell In CSNRange.Cells
            If cell.Value <> "" Then
                AttachNumber = cell.Offset(0, 1).Value - 1
                ThisWorkbook.Sheets(ReportView).Cells(i, 2).Value = "Attachment " & AttachNumber & ": " & cell
            End If
            
            i = i + 1
        Next cell
    Else
        i = 44
        ThisWorkbook.Sheets(ReportView).Cells(i - 1, 2).Value = "Attachment 2: Shipping View"
        For Each cell In CSNRange.Cells
            If cell.Value <> "" Then
                AttachNumber = cell.Offset(0, 1).Value
                ThisWorkbook.Sheets(ReportView).Cells(i, 2).Value = "Attachment " & AttachNumber & ": " & cell
            End If
            
            i = i + 1
        Next cell
    End If
End Sub

    'Add fill site ID aliquot signature line to last BPR
Sub AddSiteIDAliquots()
    Dim ls As Worksheet
    Dim cell As Range
    Dim StartHereLastRow As Long
    Dim StartHereRange As Range

    Set ls = Sheets("Start Here")
    
    StartHereLastRow = ls.Cells(ls.Rows.Count, "A").End(xlUp).Row
    Set StartHereRange = ls.Range("A" & StartHereLastRow)
    For Each cell In StartHereRange.Cells
        StartHereBPRName = cell.Value
    Next cell
End Sub

