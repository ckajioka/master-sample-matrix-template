'Purpose: Set up printing views for different sheets
'Author: Christopher Kajioka

Sub PageBreakView()
    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim NextPageBreakNumber As Long
    Dim PageBreakFirstLine  As Object
    Dim LineNumber As Long
    
    'Sets up print views for Group, MFG, and Shipping View sheets
    If InStr(CopySheetNames, ";" & ws.Name) <> 0 Or ws.Name = ShippingView Or ws.Name = ChangeView Then
    
        Application.PrintCommunication = False
        
        'Sets worksheets page margins
        With Worksheets(ws.Name).PageSetup
            .PrintTitleRows = "$1:$" & rowtofilter
            .PrintTitleColumns = ""
            .LeftMargin = Application.InchesToPoints(0.25)
            .RightMargin = Application.InchesToPoints(0.25)
            .TopMargin = Application.InchesToPoints(0.25)
            .BottomMargin = Application.InchesToPoints(1.25)
            .HeaderMargin = Application.InchesToPoints(0.55)
            .FooterMargin = Application.InchesToPoints(1)
            .PrintHeadings = False
            .PrintGridlines = False
            .PrintComments = xlPrintNoComments
            .PrintQuality = 600
            .CenterHorizontally = False
            .CenterVertically = False
            .Orientation = xlLandscape
            .Draft = False
            .PaperSize = xlPaperTabloid
            .FirstPageNumber = xlAutomatic
            .Order = xlDownThenOver
            .BlackAndWhite = False
            .Zoom = False
            .FitToPagesWide = 1
            .FitToPagesTall = 0
            .PrintErrors = xlPrintErrorsDisplayed
            .OddAndEvenPagesHeaderFooter = False
            .DifferentFirstPageHeaderFooter = False
            .ScaleWithDocHeaderFooter = True
            .AlignMarginsHeaderFooter = True
            .EvenPage.LeftHeader.Text = ""
            .EvenPage.CenterHeader.Text = ""
            .EvenPage.RightHeader.Text = ""
            .EvenPage.LeftFooter.Text = ""
            .EvenPage.CenterFooter.Text = ""
            .EvenPage.RightFooter.Text = ""
            .FirstPage.LeftHeader.Text = ""
            .FirstPage.CenterHeader.Text = ""
            .FirstPage.RightHeader.Text = ""
            .FirstPage.LeftFooter.Text = ""
            .FirstPage.CenterFooter.Text = ""
            .FirstPage.RightFooter.Text = ""
            .PrintArea = ""
        End With
        
        Application.PrintCommunication = True
        
        'Makes sure page breaks don't cut off merged cells
        ActiveWindow.View = xlPageBreakPreview                                                              'This needs to be on even though it slows the code down because if you print preview and run macro again you get an error
        ws.ResetAllPageBreaks
        NextPageBreakNumber = 1
        
        On Error Resume Next
        
        While NextPageBreakNumber <= ws.HPageBreaks.Count
            Set PageBreakFirstLine = ws.HPageBreaks(NextPageBreakNumber).Location
            
            If Err.Number > 0 Then
                Err.Number = 0
                ActiveWindow.View = xlNormalView
                Set PageBreakFirstLine = ws.HPageBreaks(NextPageBreakNumber).Location
                ActiveWindow.View = xlPageBreakPreview
            End If
            
            If Err.Number > 0 Then
                Err.Number = 0
                MsgBox ("There is a page break error on sheet: " & ActiveSheet.Name & " please manually fix after macro completes")
            End If
            
            LineNumber = PageBreakFirstLine.Row
            
            If ws.Cells(LineNumber, 1).MergeCells = True Then
                Set ws.HPageBreaks(NextPageBreakNumber).Location = ws.Cells(ws.Cells(LineNumber, 1).MergeArea.Row, 1)

                If Err.Number > 0 Then
                    Err.Number = 0
                    ActiveWindow.View = xlNormalView
                    Set ws.HPageBreaks(NextPageBreakNumber).Location = ws.Cells(ws.Cells(LineNumber, 1).MergeArea.Row, 1)
                    ActiveWindow.View = xlPageBreakPreview
                End If
                
                If Err.Number > 0 Then
                    Err.Number = 0
                    MsgBox ("There is a page break error on sheet: " & ActiveSheet.Name & " please manually fix after macro completes")
                End If
            End If
            
            NextPageBreakNumber = NextPageBreakNumber + 1
        Wend
        
        On Error GoTo 0
        
        If ActiveWindow.View = xlNormalView Then
            ActiveWindow.View = xlPageBreakPreview
        End If
        
        Application.PrintCommunication = False
        
    End If
    
    
    'Sets up print view for Report and Partner Approval sheets
    If ws.Name = ReportView Or ws.Name = PartnerView Then
    
        Application.PrintCommunication = False
        
        'Sets worksheets page margins
        With Worksheets(ws.Name).PageSetup
            .PrintTitleRows = "$1:$" & rowtofilter
            .PrintTitleColumns = ""
            .LeftMargin = Application.InchesToPoints(0.25)
            .RightMargin = Application.InchesToPoints(0.25)
            .TopMargin = Application.InchesToPoints(0.25)
            .BottomMargin = Application.InchesToPoints(1.25)
            .HeaderMargin = Application.InchesToPoints(0.55)
            .FooterMargin = Application.InchesToPoints(1)
            .PrintHeadings = False
            .PrintGridlines = False
            .PrintComments = xlPrintNoComments
            .PrintQuality = 600
            .CenterHorizontally = False
            .CenterVertically = False
            .Orientation = xlPortrait
            .Draft = False
            .PaperSize = xlPaperLetter
            .FirstPageNumber = xlAutomatic
            .Order = xlDownThenOver
            .BlackAndWhite = False
            .Zoom = False
            .FitToPagesWide = 1
            .FitToPagesTall = 0
            .PrintErrors = xlPrintErrorsDisplayed
            .OddAndEvenPagesHeaderFooter = False
            .DifferentFirstPageHeaderFooter = False
            .ScaleWithDocHeaderFooter = True
            .AlignMarginsHeaderFooter = True
            .EvenPage.LeftHeader.Text = ""
            .EvenPage.CenterHeader.Text = ""
            .EvenPage.RightHeader.Text = ""
            .EvenPage.LeftFooter.Text = ""
            .EvenPage.CenterFooter.Text = ""
            .EvenPage.RightFooter.Text = ""
            .FirstPage.LeftHeader.Text = ""
            .FirstPage.CenterHeader.Text = ""
            .FirstPage.RightHeader.Text = ""
            .FirstPage.LeftFooter.Text = ""
            .FirstPage.CenterFooter.Text = ""
            .FirstPage.RightFooter.Text = ""
            .PrintArea = ""
        End With
        
        Application.PrintCommunication = True
        ws.PageSetup.Orientation = xlPortrait
        Application.PrintCommunication = False
        
        ActiveWindow.View = xlPageBreakPreview
    End If
     
     
    'Sets up print view for Master View sheet
    If ws.Name = MasterView Then
        Dim PrintCol As Long
        Dim PrintColLetter As String

        Application.PrintCommunication = False
        
        PrintCol = MasterLastCol - 3                                                                        'Last column - 3 to remove signature boxes from print view
        PrintColLetter = Split(Cells(1, PrintCol).Address, "$")(1)                                          'Converts column number to letter
        
        'Sets worksheets page margins
        With Worksheets(ws.Name).PageSetup
            .PrintTitleRows = "$1:$" & rowtofilter
            .PrintTitleColumns = ""
            .LeftMargin = Application.InchesToPoints(0.25)
            .RightMargin = Application.InchesToPoints(0.25)
            .TopMargin = Application.InchesToPoints(0.25)
            .BottomMargin = Application.InchesToPoints(1.25)
            .HeaderMargin = Application.InchesToPoints(0.55)
            .FooterMargin = Application.InchesToPoints(1)
            .PrintHeadings = False
            .PrintGridlines = False
            .PrintComments = xlPrintNoComments
            .PrintQuality = 600
            .CenterHorizontally = False
            .CenterVertically = False
            .Orientation = xlLandscape
            .Draft = False
            .PaperSize = xlPaperTabloid
            .FirstPageNumber = xlAutomatic
            .Order = xlDownThenOver
            .BlackAndWhite = False
            .Zoom = False
            .FitToPagesWide = 1
            .FitToPagesTall = 0
            .PrintErrors = xlPrintErrorsDisplayed
            .OddAndEvenPagesHeaderFooter = False
            .DifferentFirstPageHeaderFooter = False
            .ScaleWithDocHeaderFooter = True
            .AlignMarginsHeaderFooter = True
            .EvenPage.LeftHeader.Text = ""
            .EvenPage.CenterHeader.Text = ""
            .EvenPage.RightHeader.Text = ""
            .EvenPage.LeftFooter.Text = ""
            .EvenPage.CenterFooter.Text = ""
            .EvenPage.RightFooter.Text = ""
            .FirstPage.LeftHeader.Text = ""
            .FirstPage.CenterHeader.Text = ""
            .FirstPage.RightHeader.Text = ""
            .FirstPage.LeftFooter.Text = ""
            .FirstPage.CenterFooter.Text = ""
            .FirstPage.RightFooter.Text = ""
            .PrintArea = "A1:" & PrintColLetter & MasterLastRow & ""                                             'Sets Master View print area
        End With
             
        Application.PrintCommunication = True
        
        'Makes sure page breaks don't cut off merged cells
        ActiveWindow.View = xlPageBreakPreview                                                              'This needs to be on even though it slows the code down because if you print preview and run macro again you get an error
        ws.ResetAllPageBreaks
        NextPageBreakNumber = 1
        
        On Error Resume Next
        
        While NextPageBreakNumber <= ws.HPageBreaks.Count
            Set PageBreakFirstLine = ws.HPageBreaks(NextPageBreakNumber).Location
            
            If Err.Number > 0 Then
                Err.Number = 0
                ActiveWindow.View = xlNormalView
                Set PageBreakFirstLine = ws.HPageBreaks(NextPageBreakNumber).Location
                ActiveWindow.View = xlPageBreakPreview
            End If
            
            If Err.Number > 0 Then
                Err.Number = 0
                MsgBox ("There is a page break error on sheet: " & ActiveSheet.Name & " please manually fix after macro completes")
            End If
            
            LineNumber = PageBreakFirstLine.Row
            
            If ws.Cells(LineNumber, 1).MergeCells = True Then
                Set ws.HPageBreaks(NextPageBreakNumber).Location = ws.Cells(ws.Cells(LineNumber, 1).MergeArea.Row, 1)

                If Err.Number > 0 Then
                    Err.Number = 0
                    ActiveWindow.View = xlNormalView
                    Set ws.HPageBreaks(NextPageBreakNumber).Location = ws.Cells(ws.Cells(LineNumber, 1).MergeArea.Row, 1)
                    ActiveWindow.View = xlPageBreakPreview
                End If
                
                If Err.Number > 0 Then
                    Err.Number = 0
                    MsgBox ("There is a page break error on sheet: " & ActiveSheet.Name & " please manually fix after macro completes")
                End If
            End If
            
            NextPageBreakNumber = NextPageBreakNumber + 1
        Wend
        
        On Error GoTo 0
        
        If ActiveWindow.View = xlNormalView Then
            ActiveWindow.View = xlPageBreakPreview
        End If
        
        Application.PrintCommunication = False
        
    End If
    
    ActiveSheet.DisplayPageBreaks = False

End Sub
