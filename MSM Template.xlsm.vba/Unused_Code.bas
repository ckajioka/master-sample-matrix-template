'Purpose: Code that is not called on, some of this code may be ready for use but has not been approved for use yet
'Author: Christopher Kajioka


''Highlight every other row for Master View
'Sub aaaaMasterHighlight()
'Call CopyToString
'    Dim rgToFill As Range
'    Dim i As Long
'    Dim j As Long
'    Dim d As Long
'    Dim LastRow As Long
'    Dim masterws As Worksheet
'    Set masterws = ThisWorkbook.Worksheets(MasterView)
'
'    On Error Resume Next
'
'    LastRow = masterws.Cells(Rows.Count, rowtofilter).End(xlUp).Row
'
'    i = rowtofilter + 1
'    d = 1
'
'    'Range.Interior.Color = xlNone
'    Do While i < LastRow + 1
'        j = i - 1 + masterws.Range(SLColLetter & i).MergeArea.Cells.Count
'
'        For i = i To j
'            If d Mod 2 = 0 Then
'                If rgToFill Is Nothing Then
'                    Set rgToFill = masterws.Range(SLColLetter & i & ":" & LastColLetter & i)
'                Else
'                    Set rgToFill = Union(rgToFill, masterws.Range(SLColLetter & i & ":" & LastColLetter & i))
'                End If
'
'            End If
'        Next
'
'        i = j
'        i = i + 1
'        d = d + 1
'    Loop
'
'
'    With rgToFill
'        .Interior.Pattern = xlSolid
'        .Interior.PatternColorIndex = xlAutomatic
'        .Interior.ColorIndex = 15
'    End With
'
'    If Err.Number > 0 Then
'        Err.Number = 0
'        HighlightError = True
'        HighlightErrorSheets = HighlightErrorSheets & masterws.Name & ", "
'    End If
'
'    On Error GoTo 0
'End Sub

''Old Highlight every other row for Master View (entire row), breaks pagebreak view, need to set page breaks if used.
'Sub MasterHighlight()
'    Dim rgToFill As Range
'    Dim i As Long
'    Dim j As Long
'    Dim d As Long
'    Dim LastRow As Long
'    Dim masterws As Worksheet
'    Set masterws = ThisWorkbook.Worksheets(MasterView)
'
'    On Error Resume Next
'
'    LastRow = masterws.Cells(Rows.Count, rowtofilter).End(xlUp).Row
'
'    i = rowtofilter + 1
'    d = 1
'
'    'Range.Interior.Color = xlNone
'    Do While i < LastRow + 1
'        j = i - 1 + masterws.Range(SLColLetter & i).MergeArea.Cells.Count
'
'        For i = i To j
'            If d Mod 2 = 0 Then
'                If rgToFill Is Nothing Then
'                    Set rgToFill = masterws.Range(SLColLetter & i)
'                Else
'                    Set rgToFill = Union(rgToFill, masterws.Range(SLColLetter & i))
'                End If
'
'            End If
'        Next
'
'        i = j
'        i = i + 1
'        d = d + 1
'    Loop
'
'
'    With rgToFill.EntireRow
'        .Interior.Pattern = xlSolid
'        .Interior.PatternColorIndex = xlAutomatic
'        .Interior.ColorIndex = 15
'    End With
'
'    If Err.Number > 0 Then
'        Err.Number = 0
'        HighlightError = True
'        HighlightErrorSheets = HighlightErrorSheets & masterws.Name & ", "
'    End If
'
'    On Error GoTo 0
'End Sub
'
''Highlight every other sample in every view but Master View, breaks pagebreak view, need to set page breaks if used.
''This is ready for use, but may need optimization
''Can integrate with hide rows view maybe? Should be able to.
'Sub HighlightSamples()
'    Dim ws As Worksheet
'    Set ws = ActiveSheet
'
'    If InStr(CopySheetNames, ";" & ws.Name) <> 0 Then
'        Dim rgToFill As Range
'        Dim i As Long
'        Dim j As Long
'        Dim d As Long
'        Dim LastRow As Long
'
'        On Error Resume Next
'
'        LastRow = Cells(Rows.Count, rowtofilter).End(xlUp).Row
'
'        i = rowtofilter + 1
'        d = 1
'
'        'Range.Interior.Color = xlNone
'        Do While i < LastRow + 1
'            Do While Rows(i).EntireRow.Hidden = True
'                i = i + 1
'            Loop
'
'            j = i - 1 + Range(SLColLetter & i).MergeArea.Cells.Count
'
'            For i = i To j
'                If d Mod 2 = 0 Then
'                    If rgToFill Is Nothing Then
'                        Set rgToFill = Range(SLColLetter & i)
'                    Else
'                        Set rgToFill = Union(rgToFill, Range(SLColLetter & i))
'                    End If
'
'                End If
'            Next
'
'            i = j
'            i = i + 1
'            d = d + 1
'        Loop
'
'        With rgToFill.EntireRow
'            .Interior.Pattern = xlSolid
'            .Interior.PatternColorIndex = xlAutomatic
'            .Interior.ColorIndex = 15
'        End With
'
'        If Err.Number > 0 Then
'            Err.Number = 0
'            HighlightError = True
'            HighlightErrorSheets = HighlightErrorSheets & ws.Name & ", "
'        End If
'
'        On Error GoTo 0
'
'    End If
'End Sub



''Searches BPR column and returns row for Upstream and Downstream solutions
'Sub atest()
'    Dim BPRRange As Range
'    RowValue = 0
'    Count = 0
'
'    Set BPRRange = ActiveSheet.Range(Cells(1, 2), Cells(100, 2))
'    Do While Not IsError(Application.Match("Upstream_Solutions", BPRRange, 0)) Or Not IsError(Application.Match("Downstream_Solutions", BPRRange, 0))
'        If Not IsError(Application.Match("Upstream_Solutions", BPRRange, 0)) Then
'            FoundCell = Application.Match("Upstream_Solutions", BPRRange, 0)
'            RowValue = RowValue + FoundCell
'            Count = Count + 1
'            If Cells(RowValue, 2).Offset(0, -1).MergeCells And Cells(RowValue, 2).Offset(0, -1).Value = 0 Then
'                Count = Count - 1
'            End If
'            MsgBox (Count)
'
'        ElseIf Not IsError(Application.Match("Downstream_Solutions", BPRRange, 0)) Then
'            FoundCell = Application.Match("Downstream_Solutions", BPRRange, 0)
'            RowValue = RowValue + FoundCell
'            Count = Count + 1
'            If Cells(RowValue, 2).Offset(0, -1).MergeCells And Cells(RowValue, 2).Offset(0, -1).Value = 0 Then
'                Count = Count - 1
'            End If
'            MsgBox (Count)
'
'        End If
'
'        Set BPRRange = ActiveSheet.Range(Cells(RowValue + 1, 2), Cells(100, 2))
'    Loop
'
'End Sub





''Delete hidden rows and columns
'Sub DeleteHidden()
'    'Range("A1:A300").SpecialCells(xlCellTypeVisible).EntireRow.Delete
'    Range("A1:A300").EntireRow.Hidde
'End Sub


''Delete hidden rows (not great if different Test types cause merged cells)
'Sub Del_Hidden()
'  Dim rVis As Range
'
'  Application.ScreenUpdating = False
'    Do Until ActiveSheet.Columns("A").SpecialCells(xlVisible).Count = ActiveSheet.Rows.Count
'      Set rVis = ActiveSheet.Columns("A").SpecialCells(xlVisible)
'      If rVis.Row = 1 Then
'        ActiveSheet.Rows(rVis.Areas(1).Rows.Count + 1 & ":" & rVis.Areas(2).Row - 1).Delete
'      Else
'        ActiveSheet.Rows("1:" & rVis.Row - 1).Delete
'      End If
'    Loop
'  Application.ScreenUpdating = True
'End Sub


'    Before optimization on 16Oct2018
'    'Hide MFG View Row Names
'    If InStr(MFGViewNames, ";" & ActiveSheet.Name) <> 0 Then
'        'Find column number
'        mfgfindcol1 = Application.WorksheetFunction.Match(MFG_coltofilter1, Range(ColumnRange), 0)
'        mfgfindcol2 = Application.WorksheetFunction.Match(MFG_coltofilter2, Range(ColumnRange), 0)
'        mfgfindcol3 = Application.WorksheetFunction.Match(MFG_coltofilter3, Range(ColumnRange), 0)
'        mfgfindcol4 = Application.WorksheetFunction.Match(MFG_coltofilter4, Range(ColumnRange), 0)
'        mfgfindcol5 = Application.WorksheetFunction.Match(MFG_coltofilter5, Range(ColumnRange), 0)
'        mfgfindcol6 = Application.WorksheetFunction.Match(MFG_coltofilter6, Range(ColumnRange), 0)
'        mfgfindcol7 = Application.WorksheetFunction.Match(MFG_coltofilter7, Range(ColumnRange), 0)
'
'        'Change to column letter
'        ColumnLetter1 = Split(Cells(1, mfgfindcol1).Address, "$")(1)
'        ColumnLetter2 = Split(Cells(1, mfgfindcol2).Address, "$")(1)
'        ColumnLetter3 = Split(Cells(1, mfgfindcol3).Address, "$")(1)
'        ColumnLetter4 = Split(Cells(1, mfgfindcol4).Address, "$")(1)
'        ColumnLetter5 = Split(Cells(1, mfgfindcol5).Address, "$")(1)
'        ColumnLetter6 = Split(Cells(1, mfgfindcol6).Address, "$")(1)
'        ColumnLetter7 = Split(Cells(1, mfgfindcol7).Address, "$")(1)
'
'        LastRow = Cells(Rows.Count, rowtofilter).End(xlUp).Row
'
'        'Search for merged cells and not QC_LIMS and not MFG to aliquot samples, replace text with Multiple Tests
'        For i = rowtofilter To LastRow
'            If Range(ColumnLetter1 & i).MergeArea.Cells.Count > 1 And Range(ColumnLetter2 & i) <> MFG_stringtofilter1 And Range(ColumnLetter4 & i) <> MFG_stringtofilter2 Then
'                Range(ColumnLetter3 & i).Value = "Multiple Tests"
'                Range(ColumnLetter5 & i).Value = "N/A"
'                Range(ColumnLetter6 & i).Value = "N/A"
'                Range(ColumnLetter7 & i).Value = "N/A"
'            End If
'        Next i
'
'        'Search for merged cells and not QC_LIMS and not MFG to aliquot samples, delete bottom rows of merged cell
'        i = LastRow - rowtofilter
'
'        Do While i > (rowtofilter - 1)
'
'            'Just added this on 12Oct18, remove if any problems (not needed for code to work)
'            Do While Rows(i).EntireRow.Hidden = True
'                i = i - 1
'            Loop
'
'            If Range(ColumnLetter1 & i).Value = 0 And Range(ColumnLetter2 & i) <> MFG_stringtofilter1 And Range(ColumnLetter4 & i) <> MFG_stringtofilter2 Then
'                If rgToDelete Is Nothing Then
'                    Set rgToDelete = Range(ColumnLetter1 & i)
'                Else
'                    Set rgToDelete = Union(rgToDelete, Range(ColumnLetter1 & i))
'                End If
'            End If
'            i = i - 1
'        Loop
'        rgToDelete.EntireRow.Delete
'
'         'Hide rows not needed for sheet
'        Set rangedata = Range(StartPoint).CurrentRegion
'        findcol = Application.WorksheetFunction.Match(MFG_coltofilter, Range(ColumnRange), 0)
'        rangedata.AutoFilter Field:=findcol, Criteria1:=ActiveSheet.Name
'    End If


'    'After optimization, this deletes visible rows only using old do while loop 16Oct2018
'    'Hide MFG View Row Names
'    If InStr(MFGViewNames, ";" & ActiveSheet.Name) <> 0 Then
'        'Find column number
'        mfgfindcol1 = Application.WorksheetFunction.Match(MFG_coltofilter1, Range(ColumnRange), 0)
'        mfgfindcol2 = Application.WorksheetFunction.Match(MFG_coltofilter2, Range(ColumnRange), 0)
'        mfgfindcol3 = Application.WorksheetFunction.Match(MFG_coltofilter3, Range(ColumnRange), 0)
'        mfgfindcol4 = Application.WorksheetFunction.Match(MFG_coltofilter4, Range(ColumnRange), 0)
'        mfgfindcol5 = Application.WorksheetFunction.Match(MFG_coltofilter5, Range(ColumnRange), 0)
'        mfgfindcol6 = Application.WorksheetFunction.Match(MFG_coltofilter6, Range(ColumnRange), 0)
'        mfgfindcol7 = Application.WorksheetFunction.Match(MFG_coltofilter7, Range(ColumnRange), 0)
'
'        'Change to column letter
'        ColumnLetter1 = Split(Cells(1, mfgfindcol1).Address, "$")(1)
'        ColumnLetter2 = Split(Cells(1, mfgfindcol2).Address, "$")(1)
'        ColumnLetter3 = Split(Cells(1, mfgfindcol3).Address, "$")(1)
'        ColumnLetter4 = Split(Cells(1, mfgfindcol4).Address, "$")(1)
'        ColumnLetter5 = Split(Cells(1, mfgfindcol5).Address, "$")(1)
'        ColumnLetter6 = Split(Cells(1, mfgfindcol6).Address, "$")(1)
'        ColumnLetter7 = Split(Cells(1, mfgfindcol7).Address, "$")(1)
'
'        LastRow = Cells(Rows.Count, rowtofilter).End(xlUp).Row
'
'         'Hide rows not needed for sheet, added 16Oct2018
'        Set rangedata = Range(StartPoint).CurrentRegion
'        findcol = Application.WorksheetFunction.Match(MFG_coltofilter, Range(ColumnRange), 0)
'        rangedata.AutoFilter Field:=findcol, Criteria1:=ActiveSheet.Name
'
'        'Search for merged cells and not QC_LIMS and not MFG to aliquot samples, replace text with Multiple Tests
'        For i = rowtofilter To LastRow
'            If Range(ColumnLetter1 & i).MergeArea.Cells.Count > 1 And Range(ColumnLetter2 & i) <> MFG_stringtofilter1 And Range(ColumnLetter4 & i) <> MFG_stringtofilter2 And Rows(i).EntireRow.Hidden = False Then
'                Range(ColumnLetter3 & i).Value = "Multiple Tests"
'                Range(ColumnLetter5 & i).Value = "N/A"
'                Range(ColumnLetter6 & i).Value = "N/A"
'                Range(ColumnLetter7 & i).Value = "N/A"
'            End If
'
'        Next i
'
'        'Search for merged cells and not QC_LIMS and not MFG to aliquot samples, delete bottom rows of merged cell
'        i = LastRow - rowtofilter
'
'        Do While i > (rowtofilter - 1)
'
'            'Just added this on 12Oct18, remove if any problems (not needed for code to work), removed 16Oct2018
'            Do While Rows(i).EntireRow.Hidden = True
'                i = i - 1
'            Loop
'
'            If Range(ColumnLetter1 & i).Value = 0 And Range(ColumnLetter2 & i) <> MFG_stringtofilter1 And Range(ColumnLetter4 & i) <> MFG_stringtofilter2 Then
'                If rgToDelete Is Nothing Then
'                    Set rgToDelete = Range(ColumnLetter1 & i)
'                Else
'                    Set rgToDelete = Union(rgToDelete, Range(ColumnLetter1 & i))
'                End If
'            End If
'            i = i - 1
'        Loop
'
'        'To remove autofilter, prevent unmerging cells
'        On Error Resume Next
'        ActiveSheet.ShowAllData
'        On Error GoTo 0
'
'        If Not rgToDelete Is Nothing Then
'            rgToDelete.EntireRow.Delete
'        End If
'
'        'Reset autofilter
'        rangedata.AutoFilter Field:=findcol, Criteria1:=ActiveSheet.Name
'    End If


'    'To remove autofilter on 16OCt2018.  Problem is it adds arrows again when you refilter
'    Cells.AutoFilter


'    'Optimized MFG row filter by renaming instead of deleting on 16Oct2018.  Problem is row height of merged cells after filter, cuts off information in visible cell
'    'Hide MFG View Row Names
'    If InStr(MFGViewNames, ";" & ActiveSheet.Name) <> 0 Then
'        'Find column number
'        mfgfindcol1 = Application.WorksheetFunction.Match(MFG_coltofilter1, Range(ColumnRange), 0)
'        mfgfindcol2 = Application.WorksheetFunction.Match(MFG_coltofilter2, Range(ColumnRange), 0)
'        mfgfindcol3 = Application.WorksheetFunction.Match(MFG_coltofilter3, Range(ColumnRange), 0)
'        mfgfindcol4 = Application.WorksheetFunction.Match(MFG_coltofilter4, Range(ColumnRange), 0)
'        mfgfindcol5 = Application.WorksheetFunction.Match(MFG_coltofilter5, Range(ColumnRange), 0)
'        mfgfindcol6 = Application.WorksheetFunction.Match(MFG_coltofilter6, Range(ColumnRange), 0)
'        mfgfindcol7 = Application.WorksheetFunction.Match(MFG_coltofilter7, Range(ColumnRange), 0)
'
'
'        'Added 16Oct62018
'        mfgfindcol0 = Application.WorksheetFunction.Match(MFG_coltofilter, Range(ColumnRange), 0)
'
'        'Change to column letter
'        ColumnLetter1 = Split(Cells(1, mfgfindcol1).Address, "$")(1)
'        ColumnLetter2 = Split(Cells(1, mfgfindcol2).Address, "$")(1)
'        ColumnLetter3 = Split(Cells(1, mfgfindcol3).Address, "$")(1)
'        ColumnLetter4 = Split(Cells(1, mfgfindcol4).Address, "$")(1)
'        ColumnLetter5 = Split(Cells(1, mfgfindcol5).Address, "$")(1)
'        ColumnLetter6 = Split(Cells(1, mfgfindcol6).Address, "$")(1)
'        ColumnLetter7 = Split(Cells(1, mfgfindcol7).Address, "$")(1)
'
'        'Added 16Oct62018
'        ColumnLetter0 = Split(Cells(1, mfgfindcol0).Address, "$")(1)
'
'        LastRow = Cells(Rows.Count, rowtofilter).End(xlUp).Row
'
'         'Hide rows not needed for sheet, added 16Oct2018
'        Set rangedata = Range(StartPoint).CurrentRegion
'        findcol = Application.WorksheetFunction.Match(MFG_coltofilter, Range(ColumnRange), 0)
'        rangedata.AutoFilter Field:=findcol, Criteria1:=ActiveSheet.Name
'
'        'Search for merged cells and not QC_LIMS and not MFG to aliquot samples, replace text with Multiple Tests
'        For i = rowtofilter To LastRow
'            If Range(ColumnLetter1 & i).MergeArea.Cells.Count > 1 And Range(ColumnLetter2 & i) <> MFG_stringtofilter1 And Range(ColumnLetter4 & i) <> MFG_stringtofilter2 And Rows(i).EntireRow.Hidden = False Then
'                Range(ColumnLetter3 & i).Value = "Multiple Tests"
'                Range(ColumnLetter5 & i).Value = "N/A"
'                Range(ColumnLetter6 & i).Value = "N/A"
'                Range(ColumnLetter7 & i).Value = "N/A"
'            End If
'
'            If Range(ColumnLetter1 & i).Value = 0 And Range(ColumnLetter2 & i) <> MFG_stringtofilter1 And Range(ColumnLetter4 & i) <> MFG_stringtofilter2 And Rows(i).EntireRow.Hidden = False Then
'                Range(ColumnLetter0 & i).Value = "Hidden"
'            End If
'
'        Next i
'
'        rangedata.AutoFilter Field:=findcol, Criteria1:=ActiveSheet.Name
'    End If


'Useful and quick, but autofits everything
'Sub ViewRowHeight()
'    Call CopyToString
'
'    On Error Resume Next
'    With Cells.SpecialCells(xlCellTypeVisible)
'        .EntireRow.AutoFit
'    End With
'    On Error GoTo 0
'End Sub


'Checks Master View for minimum row height, retired on 16Oct2018
'Sub MasterRowHeight()
'    Dim MasterLastRow As Long
'    Dim k As Long
'
'    ThisWorkbook.Worksheets(MasterView).Activate
'    MasterLastRow = Cells(Rows.Count, rowtofilter).End(xlUp).Row
'
'    For k = rowtofilter To MasterLastRow
'        If Rows(k & ":" & k).EntireRow.RowHeight < (MinRowHeight) Then
'            Rows(k).Rows.AutoFit
'
'            If Rows(k & ":" & k).EntireRow.RowHeight < (MinRowHeight) Then
'                Rows(k).EntireRow.RowHeight = MinRowHeight
'            End If
'        End If
'    Next k
'End Sub


'ThisWorkbook.Worksheets(MasterView).Activate