'Purpose: Copies all values listed in the "Lists" Sheet and stores them as global variables
'Author: Christopher Kajioka

Public MacroValue As Boolean
Public MasterMerged As Boolean
Public MasterColNotFound As Boolean
Public MasterSecColNotFound As Boolean
Public SampleContNotFound As Boolean
Public MinVolNotFound As Boolean
Public ProdConcNotFound As Boolean
Public ProdLossNotFound As Boolean
Public SampleDescNotFound As Boolean
Public SubStepNotFound As Boolean
Public SpecInstNotFound As Boolean
Public BTSNotFound As Boolean
Public SampleLocNotFound As Boolean
Public SampleIDNotFound As Boolean
Public DateTimeNotFound As Boolean
Public PerfNotFound As Boolean
Public VerifNotFound As Boolean
Public ProdLossFunctionError As Boolean

Public FormatOption As Boolean
Public MergeOption As Boolean
Public HighlightOption As Boolean
Public SampleLocColOption As Boolean
Public SampleIDColOption As Boolean
Public ShippingViewOption As Boolean
Public HighlightError As Boolean
Public ProdLossOption As Boolean

Public MasterView As String
Public QC_Sheet_Name As String
Public TD_Sheet_Name As String
Public EM_Sheet_Name As String
Public MS_Sheet_Name As String
Public Dispo_Sheet_Name As String

Public GroupViewNames As String
Public MFGViewNames As String

Public QC_View As String
Public EM_View As String
Public TD_View As String
Public MS_View As String
Public Dispo_View As String
Public MFG_View As String

Public colTestType As String
Public colBPR As String
Public colBatchRelease As String

Public QC_stringtofilter As String
Public TD_stringtofilter As String
Public EM_stringtofilter As String
Public MS_stringtofilter As String
Public Dispo_stringtofilter As String

Public colSampleLinker As String
Public colTestName As String
Public colAliqGroup As String
Public colAliqQty As String
Public colAliqVol As String
Public colAliqCont As String
Public colSampleCont As String
Public colMinVol As String
Public colProdConc As String
Public colProdLoss As String
Public colSampleDesc As String
Public colSubStep As String
Public colSpecInst As String
Public colBTS As String
Public colSampleLoc As String
Public colSampleID As String
Public colDateTime As String
Public colPerf As String
Public colVerif As String

Public MFG_stringtofilter1 As String
Public MFG_stringtofilter2 As String

Public ColumnRange As String
Public StartPoint As String
Public rowtofilter As Long
Public MasterLastRow As Long
Public CopySheetNames As String

Public CSNRange As Range
Public MasterLastCol As Long
Public LastColLetter As String

Public TestTypeColNumber As Long
Public BatchReleaseColNumber As Long
Public BPRColNumber As Long
Public SLColNumber As Long
Public TestNameColNumber As Long
Public AliqGroupColNumber As Long
Public AliqQtyColNumber As Long
Public AliqVolColNumber As Long
Public AliqContColNumber As Long
Public SampleContColNumber As Long
Public MinVolColNumber As Long
Public ProdConcColNumber As Long
Public ProdLossColNumber As Long
Public SampleDescColNumber As Long
Public SubStepColNumber As Long
Public SpecInstColNumber As Long
Public BTSColNumber As Long
Public SampleLocColNumber As Long
Public SampleIDColNumber As Long
Public DateTimeColNumber As Long
Public PerfColNumber As Long
Public VerifColNumber As Long

Public TestTypeColLetter As String
Public BatchReleaseColLetter As String
Public BPRColLetter As String
Public SLColLetter As String
Public TestNameColLetter As String
Public AliqGroupColLetter As String
Public AliqQtyColLetter As String
Public AliqVolColLetter As String
Public AliqContColLetter As String
Public SampleContColLetter As String
Public MinVolColLetter As String
Public ProdConcColLetter As String
Public ProdLossColLetter As String
Public SampleDescColLetter As String
Public SubStepColLetter As String
Public SpecInstColLetter As String
Public BTSColLetter As String
Public SampleLocColLetter As String
Public SampleIDColLetter As String
Public DateTimeColLetter As String
Public PerfColLetter As String
Public VerifColLetter As String

Public ListName As String

Public ShippingView As String
Public ReportView As String
Public ChangeView As String
Public PartnerView As String

Public MinRowHeight As Double

Public MissingColNames As String
Public MissingSecColNames As String
Public HighlightErrorSheets As String

Public SummaryRowCount As Long

Public StartHereBPRName As String

Sub CopyToString()
    Dim ls As Worksheet
    Dim cell As Range

    Dim GroupVLastRow As Long
    Dim MFGVLastRow As Long
    Dim QCLastRow As Long
    Dim EMLastRow As Long
    Dim TDLastRow As Long
    Dim MSLastRow As Long
    Dim DispoLastRow As Long
    Dim MFGLastRow As Long
    Dim CSNLastRow As Long
    
    Dim GroupVRange As Range
    Dim MFGVRange As Range
    Dim QCRange As Range
    Dim EMrange As Range
    Dim TDRange As Range
    Dim MSRange As Range
    Dim DispoRange As Range
    Dim MFGRange As Range
     
    Set ls = Sheets("Lists")
         
    'Pull sheet names
    MasterView = ls.Cells(6, 2)
    QC_Sheet_Name = ls.Cells(7, 2)
    EM_Sheet_Name = ls.Cells(8, 2)
    TD_Sheet_Name = ls.Cells(9, 2)
    MS_Sheet_Name = ls.Cells(10, 2)
    Dispo_Sheet_Name = ls.Cells(11, 2)
    ShippingView = ls.Cells(12, 2)
    ReportView = ls.Cells(13, 2)
    ChangeView = ls.Cells(14, 2)
    PartnerView = ls.Cells(15, 2)
    
    'Pull group sheet names
    GroupVLastRow = ls.Cells(ls.Rows.Count, "E").End(xlUp).Row
    Set GroupVRange = ls.Range("E6:E" & GroupVLastRow)
    GroupViewNames = ""
    For Each cell In GroupVRange.Cells
        GroupViewNames = GroupViewNames & ";" & cell.Value
    Next cell
    
    'Pull mfg sheet names
    MFGVLastRow = ls.Cells(ls.Rows.Count, "H").End(xlUp).Row
    Set MFGVRange = ls.Range("H6:H" & MFGVLastRow)
    MFGViewNames = ""
    For Each cell In MFGVRange.Cells
        MFGViewNames = MFGViewNames & ";" & cell.Value
    Next cell
    
    'Pull QC Column Filter Names
    QCLastRow = ls.Cells(ls.Rows.Count, "K").End(xlUp).Row
    Set QCRange = ls.Range("K6:K" & QCLastRow)
    QC_View = ""
    For Each cell In QCRange.Cells
        QC_View = QC_View & ";" & cell.Value
    Next cell
    
    'Pull EM Column Filter Names
    EMLastRow = ls.Cells(ls.Rows.Count, "L").End(xlUp).Row
    Set EMrange = ls.Range("L6:L" & EMLastRow)
    EM_View = ""
    For Each cell In EMrange.Cells
        EM_View = EM_View & ";" & cell.Value
    Next cell
    
    'Pull TD Column Filter Names
    TDLastRow = ls.Cells(ls.Rows.Count, "M").End(xlUp).Row
    Set TDRange = ls.Range("M6:M" & TDLastRow)
    TD_View = ""
    For Each cell In TDRange.Cells
        TD_View = TD_View & ";" & cell.Value
    Next cell
    
    'Pull MS Column Filter Names
    MSLastRow = ls.Cells(ls.Rows.Count, "N").End(xlUp).Row
    Set MSRange = ls.Range("N6:N" & MSLastRow)
    MS_View = ""
    For Each cell In MSRange.Cells
        MS_View = MS_View & ";" & cell.Value
    Next cell
    
    'Pull Dispo Column Filter Names
    DispoLastRow = ls.Cells(ls.Rows.Count, "O").End(xlUp).Row
    Set DispoRange = ls.Range("O6:O" & DispoLastRow)
    Dispo_View = ""
    For Each cell In DispoRange.Cells
        Dispo_View = Dispo_View & ";" & cell.Value
    Next cell

    'Pull MFG Column Filter Names
    MFGLastRow = ls.Cells(ls.Rows.Count, "P").End(xlUp).Row
    Set MFGRange = ls.Range("P6:P" & MFGLastRow)
    MFG_View = ""
    For Each cell In MFGRange.Cells
        MFG_View = MFG_View & ";" & cell.Value
    Next cell
    
    'Pull column name for row filter
    colTestType = ls.Cells(13, 19)
    colBatchRelease = ls.Cells(31, 19)
    colBPR = ls.Cells(7, 19)
    colSampleLinker = ls.Cells(6, 19)
    colTestName = ls.Cells(14, 19)
    colAliqGroup = ls.Cells(20, 19)
    colAliqQty = ls.Cells(21, 19)
    colAliqVol = ls.Cells(22, 19)
    colAliqCont = ls.Cells(23, 19)
    colSampleCont = ls.Cells(17, 19)
    colMinVol = ls.Cells(18, 19)
    colProdConc = ls.Cells(32, 19)
    colProdLoss = ls.Cells(33, 19)
    colSampleDesc = ls.Cells(9, 19)
    colSubStep = ls.Cells(10, 19)
    colSpecInst = ls.Cells(11, 19)
    colBTS = ls.Cells(12, 19)
    colSampleLoc = ls.Cells(16, 19)
    colSampleID = ls.Cells(37, 19)
    colDateTime = ls.Cells(34, 19)
    colPerf = ls.Cells(35, 19)
    colVerif = ls.Cells(36, 19)


    'Pull what to filter in each row
    QC_stringtofilter = ls.Cells(6, 22)
    EM_stringtofilter = ls.Cells(7, 22)
    TD_stringtofilter = ls.Cells(8, 22)
    MS_stringtofilter = ls.Cells(9, 22)
    Dispo_stringtofilter = ls.Cells(10, 22)
      
    'Pull value that collapses multiple tests in MFG View sheets
    MFG_stringtofilter1 = ls.Cells(6, 22)
    MFG_stringtofilter2 = ls.Cells(11, 22)
    
    'Pull row to filter value
    rowtofilter = ls.Cells(6, 25)
    
    'Pulled from row to filter and starting point
    StartPoint = ls.Cells(6, 24) & rowtofilter
    
    'Sets Column range from Master View
    MasterLastCol = Sheets(MasterView).Cells(rowtofilter, Sheets(MasterView).Columns.Count).End(xlToLeft).Column
    LastColLetter = Split(Cells(1, MasterLastCol).Address, "$")(1)
    ColumnRange = StartPoint & ":" & LastColLetter & rowtofilter
    
    'Set Column numbers from Master View
    On Error Resume Next
    TestTypeColNumber = Application.WorksheetFunction.Match(colTestType, Sheets(MasterView).Range(ColumnRange), 0)
    BatchReleaseColNumber = Application.WorksheetFunction.Match(colBatchRelease, Sheets(MasterView).Range(ColumnRange), 0)
    BPRColNumber = Application.WorksheetFunction.Match(colBPR, Sheets(MasterView).Range(ColumnRange), 0)
    SLColNumber = Application.WorksheetFunction.Match(colSampleLinker, Sheets(MasterView).Range(ColumnRange), 0)
    TestNameColNumber = Application.WorksheetFunction.Match(colTestName, Sheets(MasterView).Range(ColumnRange), 0)
    AliqGroupColNumber = Application.WorksheetFunction.Match(colAliqGroup, Sheets(MasterView).Range(ColumnRange), 0)
    AliqQtyColNumber = Application.WorksheetFunction.Match(colAliqQty, Sheets(MasterView).Range(ColumnRange), 0)
    AliqVolColNumber = Application.WorksheetFunction.Match(colAliqVol, Sheets(MasterView).Range(ColumnRange), 0)
    AliqContColNumber = Application.WorksheetFunction.Match(colAliqCont, Sheets(MasterView).Range(ColumnRange), 0)
    SampleContColNumber = Application.WorksheetFunction.Match(colSampleCont, Sheets(MasterView).Range(ColumnRange), 0)
    MinVolColNumber = Application.WorksheetFunction.Match(colMinVol, Sheets(MasterView).Range(ColumnRange), 0)
    ProdConcColNumber = Application.WorksheetFunction.Match(colProdConc, Sheets(MasterView).Range(ColumnRange), 0)
    ProdLossColNumber = Application.WorksheetFunction.Match(colProdLoss, Sheets(MasterView).Range(ColumnRange), 0)
    SampleDescColNumber = Application.WorksheetFunction.Match(colSampleDesc, Sheets(MasterView).Range(ColumnRange), 0)
    SubStepColNumber = Application.WorksheetFunction.Match(colSubStep, Sheets(MasterView).Range(ColumnRange), 0)
    SpecInstColNumber = Application.WorksheetFunction.Match(colSpecInst, Sheets(MasterView).Range(ColumnRange), 0)
    BTSColNumber = Application.WorksheetFunction.Match(colBTS, Sheets(MasterView).Range(ColumnRange), 0)
    SampleLocColNumber = Application.WorksheetFunction.Match(colSampleLoc, Sheets(MasterView).Range(ColumnRange), 0)
    SampleIDColNumber = Application.WorksheetFunction.Match(colSampleID, Sheets(MasterView).Range(ColumnRange), 0)
    DateTimeColNumber = Application.WorksheetFunction.Match(colDateTime, Sheets(MasterView).Range(ColumnRange), 0)
    PerfColNumber = Application.WorksheetFunction.Match(colPerf, Sheets(MasterView).Range(ColumnRange), 0)
    VerifColNumber = Application.WorksheetFunction.Match(colVerif, Sheets(MasterView).Range(ColumnRange), 0)

    'Change to column letter
    TestTypeColLetter = Split(Cells(1, TestTypeColNumber).Address, "$")(1)
    BatchReleaseColLetter = Split(Cells(1, BatchReleaseColNumber).Address, "$")(1)
    BPRColLetter = Split(Cells(1, BPRColNumber).Address, "$")(1)
    SLColLetter = Split(Cells(1, SLColNumber).Address, "$")(1)              '1 / 2
    TestNameColLetter = Split(Cells(1, TestNameColNumber).Address, "$")(1)            '3
    AliqGroupColLetter = Split(Cells(1, AliqGroupColNumber).Address, "$")(1)            '4
    AliqQtyColLetter = Split(Cells(1, AliqQtyColNumber).Address, "$")(1)            '5
    AliqVolColLetter = Split(Cells(1, AliqVolColNumber).Address, "$")(1)            '6
    AliqContColLetter = Split(Cells(1, AliqContColNumber).Address, "$")(1)            '7
    SampleContColLetter = Split(Cells(1, SampleContColNumber).Address, "$")(1)
    MinVolColLetter = Split(Cells(1, MinVolColNumber).Address, "$")(1)
    ProdConcColLetter = Split(Cells(1, ProdConcColNumber).Address, "$")(1)            '8
    ProdLossColLetter = Split(Cells(1, ProdLossColNumber).Address, "$")(1)
    SampleDescColLetter = Split(Cells(1, SampleDescColNumber).Address, "$")(1)
    SubStepColLetter = Split(Cells(1, SubStepColNumber).Address, "$")(1)
    SpecInstColLetter = Split(Cells(1, SpecInstColNumber).Address, "$")(1)
    BTSColLetter = Split(Cells(1, BTSColNumber).Address, "$")(1)
    SampleLocColLetter = Split(Cells(1, SampleLocColNumber).Address, "$")(1)
    SampleIDColLetter = Split(Cells(1, SampleIDColNumber).Address, "$")(1)
    DateTimeColLetter = Split(Cells(1, DateTimeColNumber).Address, "$")(1)            '9
    PerfColLetter = Split(Cells(1, PerfColNumber).Address, "$")(1)
    VerifColLetter = Split(Cells(1, VerifColNumber).Address, "$")(1)
        
    'Set Row range from Master View
    MasterLastRow = Sheets(MasterView).Cells(Rows.Count, SLColNumber).End(xlUp).Row
    
    'Can also use merge cell count, which is used in highlight sample
    If (Sheets(MasterView).Range(SLColLetter & MasterLastRow).MergeArea.Cells.Count > 1) Then
        Do While (Sheets(MasterView).Range(SLColLetter & MasterLastRow).MergeArea.Cells.Count > 1)
            MasterLastRow = MasterLastRow + 1
        Loop
        MasterLastRow = MasterLastRow - 1
    End If
    
    On Error GoTo 0
    
    'Pull copy sheet names
    CSNLastRow = ls.Cells(ls.Rows.Count, "AA").End(xlUp).Row
    Set CSNRange = ls.Range("AA6:AA" & CSNLastRow)
    CopySheetNames = ""
    For Each cell In CSNRange.Cells
        CopySheetNames = CopySheetNames & ";" & cell.Value
    Next cell
    
    'Pull min row height
    MinRowHeight = ls.Cells(6, 32)
    
    'Reset count
    SummaryRowCount = 2
    
    'Reset highlight error list
    HighlightErrorSheets = ""
    
End Sub


