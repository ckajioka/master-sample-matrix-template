'Purpose: To review sheets for needed changes
'Author: Christopher Kajioka

'Check BPR, Test Type, Dispo, and Group to Aliquot for merged cells
Sub CheckMerged()
    Dim j As Long
    Dim SLMergeCount As Long
    
    MasterMerged = False
    
    'Check if various cellls are merged and stop macro if found
    For j = rowtofilter To MasterLastRow
        If Sheets(MasterView).Range(BPRColLetter & j).MergeArea.Cells.Count > 1 Then
            MasterMerged = True
            
            ThisWorkbook.Worksheets(MasterView).Activate
            'Selects merged cells and sets view
            Range(BPRColLetter & j).Select
            ActiveWindow.ScrollRow = Selection.Row
            ActiveWindow.ScrollColumn = Selection.Column
            Exit For
            
        ElseIf Sheets(MasterView).Range(TestTypeColLetter & j).MergeArea.Cells.Count > 1 And Sheets(MasterView).Range(TestTypeColLetter & j) <> EM_stringtofilter Then
            MasterMerged = True
            
            ThisWorkbook.Worksheets(MasterView).Activate
            'Selects merged cells and sets view
            Range(TestTypeColLetter & j).Select
            ActiveWindow.ScrollRow = Selection.Row
            ActiveWindow.ScrollColumn = Selection.Column
            Exit For
            
        ElseIf Sheets(MasterView).Range(BatchReleaseColLetter & j).MergeArea.Cells.Count > 1 And Sheets(MasterView).Range(BatchReleaseColLetter & j) <> "" Then
            MasterMerged = True
            
            ThisWorkbook.Worksheets(MasterView).Activate
            'Selects merged cells and sets view
            Range(BatchReleaseColLetter & j).Select
            ActiveWindow.ScrollRow = Selection.Row
            ActiveWindow.ScrollColumn = Selection.Column
            Exit For
            
        ElseIf Sheets(MasterView).Range(AliqGroupColLetter & j).MergeArea.Cells.Count > 1 And Sheets(MasterView).Range(AliqGroupColLetter & j) <> "" Then
            MasterMerged = True
            
            ThisWorkbook.Worksheets(MasterView).Activate
            'Selects merged cells and sets view
            Range(AliqGroupColLetter & j).Select
            ActiveWindow.ScrollRow = Selection.Row
            ActiveWindow.ScrollColumn = Selection.Column
            Exit For
            
        Else
            MasterMerged = False
            
        End If
    Next j
End Sub

'Checks Master View for minimum row height
Sub MasterRowHeight()
    Dim k As Long
    
    ThisWorkbook.Worksheets(MasterView).Activate
    
    With ActiveSheet.Cells.Rows(rowtofilter & ":" & MasterLastRow)
        'May remove do to autofit zoom issues (when you zoom out)
        .WrapText = True
        .EntireRow.AutoFit
        
        For k = rowtofilter To MasterLastRow
            If Rows(k & ":" & k).EntireRow.RowHeight < MinRowHeight Then
                Rows(k).EntireRow.RowHeight = MinRowHeight
            End If
        Next k
    End With
End Sub

'Manually run to set row height in templates
Sub TemplateRowHeight()
    Application.ScreenUpdating = False
    Dim k As Long
    
    Call CopyToString
    
    ThisWorkbook.Worksheets(MasterView).Activate
    
    With ActiveSheet.Cells.Rows(rowtofilter & ":" & MasterLastRow)
        .WrapText = True
        .EntireRow.AutoFit
        
        For k = rowtofilter To MasterLastRow
            If Rows(k & ":" & k).EntireRow.RowHeight < MinRowHeight Then
                Rows(k).EntireRow.RowHeight = MinRowHeight
            End If
        Next k
    End With
    
    Application.ScreenUpdating = True
End Sub


'Check column header names before running code, to prevent missing information and view errors
Sub MasterColumnCheck()
    MasterColNotFound = False
    MasterSecColNotFound = False
    SampleContNotFound = False
    MinVolNotFound = False
    ProdConcNotFound = False
    ProdLossNotFound = False
    SampleDescNotFound = False
    SubStepNotFound = False
    SpecInstNotFound = False
    BTSNotFound = False
    SampleLocNotFound = False
    SampleIDNotFound = False
    DateTimeNotFound = False
    PerfNotFound = False
    VerifNotFound = False
        
    MissingColNames = ""
    MissingSecColNames = ""
    
    On Error Resume Next
    
    If IsError(Application.WorksheetFunction.Match(colTestType, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colTestType & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colBatchRelease, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colBatchRelease & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colBPR, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colBPR & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colSampleLinker, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colSampleLinker & ", "
    End If
    
    If IsError(Application.WorksheetFunction.Match(colTestName, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colTestName & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colAliqGroup, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colAliqGroup & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colAliqQty, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colAliqQty & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colAliqVol, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colAliqVol & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colAliqCont, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        MissingColNames = MissingColNames & colAliqCont & ", "
    End If
    
    
    If IsError(Application.WorksheetFunction.Match(colDateTime, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MasterColNotFound = True
        DateTimeNotFound = True
        MissingColNames = MissingColNames & colDateTime & ", "
    End If
    
    'These are only neccessary for the auto merge function.  The date and time column is needed for the EM merge and un-merge as well so moved above
    If IsError(Application.WorksheetFunction.Match(colSampleCont, Sheets(MasterView).Range(ColumnRange), 0)) Then
        SampleContNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colSampleCont & ", "
    End If
    
    If IsError(Application.WorksheetFunction.Match(colMinVol, Sheets(MasterView).Range(ColumnRange), 0)) Then
        MinVolNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colMinVol & ", "
    End If
    
    If IsError(Application.WorksheetFunction.Match(colProdConc, Sheets(MasterView).Range(ColumnRange), 0)) Then
        ProdConcNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colProdConc & ", "
    End If
    
    If IsError(Application.WorksheetFunction.Match(colProdLoss, Sheets(MasterView).Range(ColumnRange), 0)) Then
        ProdLossNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colProdLoss & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colSampleDesc, Sheets(MasterView).Range(ColumnRange), 0)) Then
        SampleDescNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colSampleDesc & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colSubStep, Sheets(MasterView).Range(ColumnRange), 0)) Then
        SubStepNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colSubStep & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colSpecInst, Sheets(MasterView).Range(ColumnRange), 0)) Then
        SpecInstNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colSpecInst & ", "
    End If
    
    If IsError(Application.WorksheetFunction.Match(colBTS, Sheets(MasterView).Range(ColumnRange), 0)) Then
        BTSNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colBTS & ", "
    End If
    
    If IsError(Application.WorksheetFunction.Match(colSampleLoc, Sheets(MasterView).Range(ColumnRange), 0)) Then
        SampleLocNotFound = True
'        MasterSecColNotFound = True
'        MissingSecColNames = MissingSecColNames & colSampleLoc & ", "
    End If
    
    If IsError(Application.WorksheetFunction.Match(colSampleID, Sheets(MasterView).Range(ColumnRange), 0)) Then
        SampleIDNotFound = True
'        MasterSecColNotFound = True
'        MissingSecColNames = MissingSecColNames & colSampleID & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colPerf, Sheets(MasterView).Range(ColumnRange), 0)) Then
        PerfNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colPerf & ", "
    End If

    If IsError(Application.WorksheetFunction.Match(colVerif, Sheets(MasterView).Range(ColumnRange), 0)) Then
        VerifNotFound = True
        MasterSecColNotFound = True
        MissingSecColNames = MissingSecColNames & colVerif & ", "
    End If

    On Error GoTo 0
    
End Sub

'Count samples in each view and reports to Macro Summary sheet
Sub SampleCount()
    Dim SLRange As Range
    Dim SLCount As Long
    Dim ThisSheetName As String
       
    If SummaryRowCount = 2 Then
        ThisWorkbook.Worksheets("Macro Summary").Range("A2:B50").Clear
    End If
    
    If ActiveSheet.Name = MasterView Then
        Dim BPRRange As Range
        Dim BPRCount1 As Long
        Dim BPRCount2 As Long
        Dim RowValue As Long
        Dim FoundCell As Long
        Dim RowCount As Long
        
        ThisWorkbook.Worksheets("Macro Summary").Cells(SummaryRowCount, 3).Clear
        ThisWorkbook.Worksheets("Macro Summary").Cells(SummaryRowCount, 5).Clear

        Set SLRange = ActiveSheet.Range(Cells(rowtofilter + 1, SLColNumber), Cells(MasterLastRow, SLColNumber))
        SLCount = Application.WorksheetFunction.Subtotal(3, SLRange)

        Set BPRRange = ActiveSheet.Range(Cells(1, BPRColNumber), Cells(MasterLastRow, BPRColNumber))
        
        RowValue = 0
        BPRCount = 0
    
       'Searches BPR column and returns row for Upstream and Downstream solutions, subtracts count for sample linker merged cells, and gives total count
        Do While Not IsError(Application.Match("Upstream_Solutions", BPRRange, 0))
            If Not IsError(Application.Match("Upstream_Solutions", BPRRange, 0)) Then
                FoundCell = Application.Match("Upstream_Solutions", BPRRange, 0)
                RowValue = RowValue + FoundCell
                BPRCount = BPRCount + 1
                
                If Cells(RowValue, SLColNumber).MergeCells And Cells(RowValue, SLColNumber).Value = 0 Then
                    BPRCount = BPRCount - 1
                End If
            End If
            
            Set BPRRange = ActiveSheet.Range(Cells(RowValue + 1, BPRColNumber), Cells(MasterLastRow, BPRColNumber))
            
            If (RowValue + 1) > MasterLastRow Then
                Exit Do
            End If
        Loop
        
        Set BPRRange = ActiveSheet.Range(Cells(1, BPRColNumber), Cells(MasterLastRow, BPRColNumber))
        RowValue = 0

        Do While Not IsError(Application.Match("Downstream_Solutions", BPRRange, 0))
            If Not IsError(Application.Match("Downstream_Solutions", BPRRange, 0)) Then
                FoundCell = Application.Match("Downstream_Solutions", BPRRange, 0)
                RowValue = RowValue + FoundCell
                BPRCount = BPRCount + 1
                If Cells(RowValue, SLColNumber).MergeCells And Cells(RowValue, SLColNumber).Value = 0 Then
                    BPRCount = BPRCount - 1
                End If
            End If

            Set BPRRange = ActiveSheet.Range(Cells(RowValue + 1, BPRColNumber), Cells(MasterLastRow, BPRColNumber))

            If (RowValue + 1) > MasterLastRow Then
                Exit Do
            End If
        Loop

        ThisSheetName = ActiveSheet.Name
        ThisWorkbook.Worksheets("Macro Summary").Cells(SummaryRowCount, 3) = SLCount
        ThisWorkbook.Worksheets("Macro Summary").Cells(SummaryRowCount, 5) = BPRCount
    End If
          
    If InStr(CopySheetNames, ";" & ActiveSheet.Name) <> 0 Then
        Set SLRange = ActiveSheet.Range(Cells(rowtofilter + 1, SLColNumber), Cells(MasterLastRow, SLColNumber))
        SLCount = Application.WorksheetFunction.Subtotal(103, SLRange)
        
        ThisSheetName = ActiveSheet.Name
        ThisWorkbook.Worksheets("Macro Summary").Cells(SummaryRowCount, 1) = ThisSheetName
        ThisWorkbook.Worksheets("Macro Summary").Cells(SummaryRowCount, 2) = SLCount
        
        SummaryRowCount = SummaryRowCount + 1
    End If
End Sub