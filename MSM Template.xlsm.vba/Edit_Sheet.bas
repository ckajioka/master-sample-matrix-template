'Author: Christopher Kajioka
'Purpose: All settings that can be turned on or off that edit the content of a page

'Remove highlight samples in Master View
Sub RemoveHighlight()
    Dim masterws As Worksheet
    Set masterws = ThisWorkbook.Worksheets(MasterView)
    
    'Removes all fill
    With masterws
        .Range((rowtofilter + 1) & ":" & MasterLastRow).Interior.Color = xlNone
    End With
End Sub

'Highlight every other row for Master View
Sub MasterHighlight()
    Dim rgToFill As Range
    Dim i As Long
    Dim j As Long
    Dim d As Long
    Dim OnlyOneRow As Boolean
    Dim LastFillColNumber As Long
    Dim LastFillColLetter As String
    Dim masterws As Worksheet
    Set masterws = ThisWorkbook.Worksheets(MasterView)
    

    On Error Resume Next

    i = rowtofilter + 1
    d = 1
    OnlyOneRow = True
    
    LastFillColNumber = MasterLastCol - 3
    LastFillColLetter = Split(Cells(1, LastFillColNumber).Address, "$")(1)
    
    'Range.Interior.Color = xlNone
    Do While i < MasterLastRow + 1
        j = i - 1 + masterws.Range(SLColLetter & i).MergeArea.Cells.Count

        For i = i To j
            If d Mod 2 = 0 Then
                If rgToFill Is Nothing Then
                    OnlyOneRow = False
                    Set rgToFill = masterws.Range(SLColLetter & i & ":" & LastFillColLetter & i)
                Else
                    Set rgToFill = Union(rgToFill, masterws.Range(SLColLetter & i & ":" & LastFillColLetter & i))
                End If

            End If
        Next

        i = j
        i = i + 1
        d = d + 1
    Loop
    
    If OnlyOneRow = False Then
        With rgToFill
            .Interior.Pattern = xlSolid
            .Interior.PatternColorIndex = xlAutomatic
            '.Interior.ColorIndex = 15
            .Interior.Color = RGB(224, 224, 224)
        End With
    End If

    If Err.Number > 0 Then
        Err.Number = 0
        HighlightError = True
        HighlightErrorSheets = HighlightErrorSheets & masterws.Name & ", "
    End If
    
    On Error GoTo 0
End Sub


'Highlight every other sample in every view but Master View
'This is ready for use, but may need optimization
'Can integrate with hide rows view maybe? Should be able to.
Sub HighlightSamples()
    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    If InStr(CopySheetNames, ";" & ws.Name) <> 0 Then
        Dim rgToFill As Range
        Dim i As Long
        Dim j As Long
        Dim d As Long
        Dim LastRow As Long
        Dim OnlyOneRow As Boolean
        Dim LastFillColNumber As Long
        Dim LastFillColLetter As String
        
        On Error Resume Next
        
        LastRow = Cells(Rows.Count, SLColNumber).End(xlUp).Row
        
        If Range(SLColLetter & LastRow).MergeArea.Cells.Count > 1 Then
            LastRow = LastRow - 1 + Range(SLColLetter & LastRow).MergeArea.Cells.Count
        End If
        
        i = rowtofilter + 1
        d = 1
        OnlyOneRow = True
        
        LastFillColNumber = MasterLastCol - 3
        LastFillColLetter = Split(Cells(1, LastFillColNumber).Address, "$")(1)
        
        'Range.Interior.Color = xlNone
        Do While i < LastRow + 1
            Do While Rows(i).EntireRow.Hidden = True
                i = i + 1
            Loop
    
            j = i - 1 + Range(SLColLetter & i).MergeArea.Cells.Count
    
            For i = i To j
                If d Mod 2 = 0 Then
                    If rgToFill Is Nothing Then
                        OnlyOneRow = False
                        Set rgToFill = Range(SLColLetter & i & ":" & LastFillColLetter & i)
                    Else
                        Set rgToFill = Union(rgToFill, Range(SLColLetter & i & ":" & LastFillColLetter & i))
                    End If
    
                End If
            Next
    
            i = j
            i = i + 1
            d = d + 1
        Loop
    
        If OnlyOneRow = False Then
            With rgToFill
                .Interior.Pattern = xlSolid
                .Interior.PatternColorIndex = xlAutomatic
                '.Interior.ColorIndex = 15
                .Interior.Color = RGB(224, 224, 224)
            End With
        End If
    
        If Err.Number > 0 Then
            Err.Number = 0
            HighlightError = True
            HighlightErrorSheets = HighlightErrorSheets & ws.Name & ", "
        End If
        
        On Error GoTo 0
        
    End If
End Sub

'Formats each cell in Master View.  Note: run after auto merge
Sub AutoFormat()
    Dim masterws As Worksheet
    Set masterws = ThisWorkbook.Worksheets(MasterView)
    
    'Sets borders around MasterView Range
    With masterws.Range("A" & rowtofilter & ":" & LastColLetter & MasterLastRow)
        .Borders.LineStyle = xlContinuous
        .Borders.ColorIndex = 0
        .Borders.TintAndShade = 0
        .Borders.Weight = xlThin
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .WrapText = True
    End With
End Sub

'Check if Product Conc, product loss, and all signature fields match sample linker merged cell.  If not merge cells.
Sub AutoMerge()
    
    On Error Resume Next
    
    Dim masterws As Worksheet
    Dim rgEMMerge As Range
    Dim rg As Range
    Dim EMLastColNumber As Long
    Dim EMLastColLetter As String
    Dim i As Long
    Dim j As Long
    Dim o As Long
      
    Set masterws = ThisWorkbook.Worksheets(MasterView)
        
    i = rowtofilter + 1
        
    'Prevent merge alert confirmations
    Application.DisplayAlerts = False
    
    'Set merged column location for EM remerge
    EMLastColNumber = DateTimeColNumber - 1
    EMLastColLetter = Split(Cells(1, EMLastColNumber).Address, "$")(1)
    
    'Un-merge EM merged row first, which prevents unmerging unwated cells within the EM merge range
    For o = (rowtofilter + 1) To MasterLastRow
        If masterws.Range(TestTypeColLetter & o).MergeArea.Cells.Count > 1 And masterws.Range(TestTypeColLetter & o).Value = EM_stringtofilter Then
            masterws.Range(TestTypeColLetter & o).UnMerge
            
            If rgEMMerge Is Nothing Then
                Set rgEMMerge = masterws.Range(TestTypeColLetter & o & ":" & EMLastColLetter & o)
            Else
                Set rgEMMerge = Union(rgEMMerge, masterws.Range(TestTypeColLetter & o & ":" & EMLastColLetter & o))
            End If
            
            'Put "N/A" in for these fields if EM sample
            With masterws
                .Range(DateTimeColLetter & o).Value = "N/A"
                .Range(PerfColLetter & o).Value = "N/A"
                .Range(VerifColLetter & o).Value = "N/A"
            End With
            
        End If
    Next o
    
    'Un-merge cells and center
    With masterws
        If SampleContNotFound = False Then
            .Range(SampleContColLetter & i & ":" & SampleContColLetter & MasterLastRow).UnMerge
        End If
        
        If MinVolNotFound = False Then
            .Range(MinVolColLetter & i & ":" & MinVolColLetter & MasterLastRow).UnMerge
        End If
        
        If ProdConcNotFound = False Then
            .Range(ProdConcColLetter & i & ":" & ProdConcColLetter & MasterLastRow).UnMerge
        End If
        
        If ProdLossNotFound = False Then
            .Range(ProdLossColLetter & i & ":" & ProdLossColLetter & MasterLastRow).UnMerge
        End If
        
        If SampleDescNotFound = False Then
            .Range(SampleDescColLetter & i & ":" & SampleDescColLetter & MasterLastRow).UnMerge
        End If
        
        If SubStepNotFound = False Then
            .Range(SubStepColLetter & i & ":" & SubStepColLetter & MasterLastRow).UnMerge
        End If
        
        If SpecInstNotFound = False Then
            .Range(SpecInstColLetter & i & ":" & SpecInstColLetter & MasterLastRow).UnMerge
        End If
        
        If BTSNotFound = False Then
            .Range(BTSColLetter & i & ":" & BTSColLetter & MasterLastRow).UnMerge
        End If
        
        If SampleLocNotFound = False Then
            .Range(SampleLocColLetter & i & ":" & SampleLocColLetter & MasterLastRow).UnMerge
        End If
        
        If SampleIDNotFound = False Then
            .Range(SampleIDColLetter & i & ":" & SampleIDColLetter & MasterLastRow).UnMerge
        End If
        
        If DateTimeNotFound = False Then
            .Range(DateTimeColLetter & i & ":" & DateTimeColLetter & MasterLastRow).UnMerge
        End If
        
        If PerfNotFound = False Then
            .Range(PerfColLetter & i & ":" & PerfColLetter & MasterLastRow).UnMerge
        End If
        
        If VerifNotFound = False Then
            .Range(VerifColLetter & i & ":" & VerifColLetter & MasterLastRow).UnMerge
        End If
        
    End With
    
    'Merge relevant cells
    Do While i < MasterLastRow + 1
        If masterws.Range(SLColLetter & i).MergeArea.Cells.Count > 1 And masterws.Range(SLColLetter & i) <> "" Then
            j = i - 1 + masterws.Range(SLColLetter & i).MergeArea.Cells.Count
            
            With masterws
                If SampleContNotFound = False Then
                    .Range(SampleContColLetter & i & ":" & SampleContColLetter & j).Merge
                End If
                
                If MinVolNotFound = False Then
                    .Range(MinVolColLetter & i & ":" & MinVolColLetter & j).Merge
                End If
                
                If ProdConcNotFound = False Then
                    .Range(ProdConcColLetter & i & ":" & ProdConcColLetter & j).Merge
                End If
                
                If ProdLossNotFound = False Then
                    .Range(ProdLossColLetter & i & ":" & ProdLossColLetter & j).Merge
                End If
                
                If SampleDescNotFound = False Then
                    .Range(SampleDescColLetter & i & ":" & SampleDescColLetter & j).Merge
                End If
                
                If SubStepNotFound = False Then
                    .Range(SubStepColLetter & i & ":" & SubStepColLetter & j).Merge
                End If
                
                If SpecInstNotFound = False Then
                    .Range(SpecInstColLetter & i & ":" & SpecInstColLetter & j).Merge
                End If
                
                If BTSNotFound = False Then
                    .Range(BTSColLetter & i & ":" & BTSColLetter & j).Merge
                End If
                
                If SampleLocNotFound = False Then
                    .Range(SampleLocColLetter & i & ":" & SampleLocColLetter & j).Merge
                End If
                
                If SampleIDNotFound = False Then
                    .Range(SampleIDColLetter & i & ":" & SampleIDColLetter & j).Merge
                End If
                
                If DateTimeNotFound = False Then
                    .Range(DateTimeColLetter & i & ":" & DateTimeColLetter & j).Merge
                End If
                
                If PerfNotFound = False Then
                    .Range(PerfColLetter & i & ":" & PerfColLetter & j).Merge
                End If
                
                If VerifNotFound = False Then
                    .Range(VerifColLetter & i & ":" & VerifColLetter & j).Merge
                End If
                
            End With
     
            i = j

        End If

        i = i + 1

    Loop
    
    'Remerge EM cells
    If Not rgEMMerge Is Nothing Then
        For Each rg In rgEMMerge.Rows
            rg.Merge
        Next
    End If

    Application.DisplayAlerts = True
    
    If Err.Number > 0 Then
        Err.Number = 0
        MsgBox ("The merged cell function failed, please disable auto merge option and inform Chris Kajioka of the issue.")
    End If
    
    On Error GoTo 0
    
End Sub

'Option to add or remove shipping page
Sub HideShippingView()
    If ShippingViewOption = False Then
        ThisWorkbook.Sheets(ShippingView).Visible = False
    Else
        ThisWorkbook.Sheets(ShippingView).Visible = True
    End If
End Sub

'Option to automatically add product loss forumula
Sub AddProdLossFormula()
    On Error Resume Next
    
    Dim masterws As Worksheet
    Dim i As Long
    Dim j As Long

    Set masterws = ThisWorkbook.Worksheets(MasterView)
        
    i = rowtofilter + 1
    ProdLossFunctionError = True
    
    If MinVolNotFound = False And ProdConcNotFound = False And ProdLossNotFound = False And TestTypeNotFound = False Then
        ProdLossFunctionError = False
        Do While i < MasterLastRow + 1
        
            If masterws.Range(SLColLetter & i).MergeArea.Cells.Count > 1 And masterws.Range(SLColLetter & i) <> "" Then
                j = masterws.Range(SLColLetter & i).MergeArea.Cells.Count - 1
            End If
    
            If masterws.Range(TestTypeColLetter & i).MergeArea.Cells.Count > 1 And masterws.Range(TestTypeColLetter & i).Value = EM_stringtofilter Then
                i = i + 1
            Else
                With masterws
                        .Cells(i, ProdLossColNumber).Value = "=(" & ProdConcColLetter & i & "*" & MinVolColLetter & i & ") / 1000"
                End With
                
                i = i + 1 + j
                j = 0
            End If
        Loop
        
        With masterws.Range(ProdLossColLetter & rowtofilter & ":" & ProdLossColLetter & MasterLastRow)
            .NumberFormat = "0.0"
        End With
    End If
    
    If Err.Number > 0 Then
        Err.Number = 0
        MsgBox ("The Add Prodct Loss formula function failed, please disable auto Add Product Loss Forumula function and inform Chris Kajioka of the issue.")
    End If

    On Error GoTo 0
    
End Sub

'Highlight every MFG located within the Group to aliquot column on the Master View
Sub GroupToAliquotHighlight()
    Dim rgToBoldRed As Range
    Dim i As Long
    Dim masterws As Worksheet
    Set masterws = ThisWorkbook.Worksheets(MasterView)

    i = rowtofilter + 1
   
    Do While i < MasterLastRow + 1
        If masterws.Range(AliqGroupColLetter & i).Value = "MFG" Then
            If rgToBoldRed Is Nothing Then
                Set rgToBoldRed = Range(AliqGroupColLetter & i)
            Else
                Set rgToBoldRed = Union(rgToBoldRed, Range(AliqGroupColLetter & i))
            End If
        End If
        
        i = i + 1
    Loop
    
    With rgToBoldRed
        .Font.Color = vbRed
        .Font.Bold = True
    End With

End Sub