'Purpose: Call certian macros on worksheet view
'Author: Christopher Kajioka

Private Sub Workbook_SheetActivate(ByVal foo As Object)
           
    'This runs when hitting "Run All Macros"
    If MacroValue = True Then
        ActiveSheet.DisplayPageBreaks = False
        ActiveWindow.View = xlNormalView

        Call CopyMaster
        Call HideColumns
        Call HideFilters
        Call HideRows
        
        If HighlightOption = True Then
            Call HighlightSamples
        End If
        
        Call PageBreakView
        Call SampleCount
        
    End If
End Sub

'Private Sub Workbook_NewSheet(ByVal Sh As Object)
'End Sub