'Purpose: Hides columns for different views on different sheets.  Also calls AddAttachName and/or AddBatchID for each sheet.
'Author: Christopher Kajioka

Sub HideColumns()
    Dim c As Range
    
    'Hide QC Columns and Add Attachment Name
    If ActiveSheet.Name = QC_Sheet_Name Then
        For Each c In Range(ColumnRange).Cells
            If Not IsError(Application.Match(c.Value, Split(QC_View, ";"), 0)) Then
                c.EntireColumn.Hidden = True
            End If
        Next c
        Call AddAttachName
    End If
        
    'Hide TD Columns
    If ActiveSheet.Name = TD_Sheet_Name Then
        For Each c In Range(ColumnRange).Cells
            If Not IsError(Application.Match(c.Value, Split(TD_View, ";"), 0)) Then
                c.EntireColumn.Hidden = True
            End If
        Next c
        Call AddAttachName
    End If
        
    'Hide EM Columns
    If ActiveSheet.Name = EM_Sheet_Name Then
        For Each c In Range(ColumnRange).Cells
            If Not IsError(Application.Match(c.Value, Split(EM_View, ";"), 0)) Then
                c.EntireColumn.Hidden = True
            End If
        Next c
        Call AddAttachName
        Call AddEMBorder
    End If
       
    'Hide MS Columns
    If ActiveSheet.Name = MS_Sheet_Name Then
        For Each c In Range(ColumnRange).Cells
            If Not IsError(Application.Match(c.Value, Split(MS_View, ";"), 0)) Then
                c.EntireColumn.Hidden = True
            End If
        Next c
        Call AddAttachName
    End If
    
    'Hide Dispo Columns
    If ActiveSheet.Name = Dispo_Sheet_Name Then
        For Each c In Range(ColumnRange).Cells
            If Not IsError(Application.Match(c.Value, Split(Dispo_View, ";"), 0)) Then
                c.EntireColumn.Hidden = True
            End If
        Next c
        Call AddAttachName
    End If
    
    'Hide MFG Sheet Columns
    If InStr(MFGViewNames, ";" & ActiveSheet.Name) <> 0 Then
        For Each c In Range(ColumnRange).Cells
            If Not IsError(Application.Match(c.Value, Split(MFG_View, ";"), 0)) Then
                c.EntireColumn.Hidden = True
            End If
        Next c
        Call AddAttachName
        Call AddBatchID
    End If
End Sub
