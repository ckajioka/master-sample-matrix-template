'Purpose: GUI Buttons located in the "Start Here" tab, used to control macros
'Author: Christopher Kajioka

'Pull value from merge option checkbox
Sub MergeOptionButton()
    MergeOption = Sheets("Start Here").Auto_Merge.Value
End Sub

'Pull value from format option checkbox
Sub FormatOptionButton()
    FormatOption = Sheets("Start Here").Auto_Format.Value
End Sub

'Pull value from highlight option checkbox
Sub HighlightOptionButton()
    HighlightOption = Sheets("Start Here").Auto_Highlight.Value
End Sub

'Pull value from sample location column option checkbox
Sub SampleLocColOptionButton()
    SampleLocColOption = Sheets("Start Here").Sample_Loc_Col_Check.Value
End Sub

'Pull value from sample id column option checkbox
Sub SampleIDColOptionButton()
    SampleIDColOption = Sheets("Start Here").Sample_ID_Col_Check.Value
End Sub

Sub ShippingViewOptionButton()
    ShippingViewOption = Sheets("Start Here").Shipping_View_Check.Value
End Sub

Sub ProdLossOptionButton()
    ProdLossOption = Sheets("Start Here").Prod_Loss_Check.Value
End Sub

'Runs the macro through each sheet
Sub CompleteRunMacro()
    Application.Calculation = xlCalculationManual
    Application.ScreenUpdating = False
    Application.DisplayStatusBar = False
    
    'Timer initiation
    Dim t As Single
    t = Timer
     
    MacroValue = True
    
    HighlightError = False
    
    Dim ws As Worksheet
    Dim starting_ws As Worksheet
    Set starting_ws = ActiveSheet
    
    Call CopyToString
    Call CheckMerged
    Call MasterColumnCheck
    Call MergeOptionButton
    Call FormatOptionButton
    Call HighlightOptionButton
    Call SampleLocColOptionButton
    Call SampleIDColOptionButton
    Call ShippingViewOptionButton
    Call HideShippingView
    Call ProdLossOptionButton
   ' Call AddSiteIDAliquots
    
    'Checks for merged cells before running
    If MasterMerged = False And MasterColNotFound = False Then
    
        Call RemoveHighlight
        
        If MergeOption = True Then
            Call AutoMerge
        End If
        
        If FormatOption = True Then
            Call AutoFormat
        End If
        
        If ProdLossOption = True Then
            Call AddProdLossFormula
        End If
        
        Call GroupToAliquotHighlight
        
        'Runs all macros by going through each sheet and Activating.  Since code is based on activation it goes through each sheet one by one
        Call AddSheets
        For Each ws In ThisWorkbook.Worksheets
            ws.Activate
        Next
        
        If HighlightOption = True Then
            Call MasterHighlight
        End If
    
        starting_ws.Activate
    End If

    Call ReportAddAttach
    
    MacroValue = False
     
    Application.DisplayStatusBar = True
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
       
    'Error message if merged cells found
    If MasterMerged = True Then
        MsgBox ("No vertically merged cells allowed in BPR, Test Type, Required for Batch Release?, or Group to Aliquot columns.  Macro ended.  See selected cell for issue.")
        
    'Error message if column not found
    ElseIf MasterColNotFound = True Then
        MsgBox ("Macro ended early. Please check the following column header names: " & MissingColNames)
        
    Else
        'Timer End, shows how long macro took
        MsgBox "Done, Macro took " & Timer - t & " seconds."
    End If
    
    'If secondary columns name not found and merge option is enabled then display error
    If MasterSecColNotFound = True And MergeOption = True Then
        MsgBox ("The following column(s) are missing from the Master View: " & MissingSecColNames & ". The auto-merge feature will not work for the column(s) listed. " _
        + "If that column is no longer needed, ask Chris Kajioka to remove from MSM code.")
    End If
    
    'If Sample location is not found when enabled then display error
    If SampleLocNotFound = True And SampleLocColOption = True Then
        MsgBox ("The " & colSampleLoc & " column is missing from the Master View but you have the option ENABLED.  If you are not using " & colSampleLoc & " then please UN-CHECK this option.")
    ElseIf SampleLocNotFound = False And SampleLocColOption = False Then
        MsgBox ("The " & colSampleLoc & " column is included in the Master View but you have the option DISABLED.  If you are using " & colSampleLoc & " then please CHECK this option.")
    End If
    
    'If Sample ID is not found when enabled then display error
    If SampleIDNotFound = True And SampleIDColOption = True Then
        MsgBox ("The " & colSampleID & " column is missing from the Master View but you have the option ENABLED.  If you are not using " & colSampleID & " then please UN-CHECK this option.")
    ElseIf SampleIDNotFound = False And SampleIDColOption = False Then
        MsgBox ("The " & colSampleID & " column is included in the Master View but you have the option DISABLED.  If you are using " & colSampleID & " then please CHECK this option.")
    End If
    
    'If there was an issue with the highlight error then display error
    If HighlightError = True Then
        MsgBox ("Auto-highlight every other row had errors on sheet(s): " & HighlightErrorSheets & ". Please confirm the view and disable this option if you continue to have issues.")
    End If
    
    'If there was an issue with the prod loss funtion then display error
    If ProdLossFunctionError = True Then
        MsgBox ("The Auto Product Loss function did not run because one or more of the following columns is missing: " & colTestType & ", " & colMinVol & ", " & colProdConc & ", or " & colProdLoss & ".")
    End If
End Sub

'Adds a Master View template based off hidden sheets
Sub AddTemplates()
    Dim MessagePrompt As String
    Dim currentws As Worksheet
    Dim wsfound As Boolean
    
    MessagePrompt = MsgBox("This will delete all the current information in the Master View. Are you sure want to load a new template?", vbQuestion + vbYesNo, "WARNING!!!!")
    wsfound = False
    
    If MessagePrompt = vbYes And Sheets("Start Here").Shapes("Combo Box").ControlFormat <> 1 Then
        Application.Calculation = xlCalculationManual
        Application.ScreenUpdating = False
        Application.DisplayStatusBar = False
    
        With Sheets("Start Here").Shapes("Combo Box").ControlFormat
            ListName = .List(.ListIndex)
        End With
        
        For Each currentws In ThisWorkbook.Worksheets
            If currentws.Name = ListName Then
                wsfound = True
                Call CopyToString
                Call CopyTemplate
                Exit For
            End If
        Next
        
        If wsfound = False Then
            MsgBox ("Sheet not found.  Please make sure the template sheet name matches the drop-down list name.")
        End If
        
        Application.DisplayStatusBar = True
        Application.ScreenUpdating = True
        Application.Calculation = xlCalculationAutomatic
    End If
End Sub

'Run Master View Row Auto-Fit
Sub RowAutoFit()
    Application.Calculation = xlCalculationManual
    Application.ScreenUpdating = False
    Application.DisplayStatusBar = False
    
    Dim starting_ws As Worksheet
    Set starting_ws = ActiveSheet

    Call CopyToString
    Call MasterColumnCheck
    
    If MasterColNotFound = False Then
        Call MasterRowHeight
        starting_ws.Activate
        MsgBox ("Auto-fit complete and min. row height set to " & MinRowHeight)
    Else
        starting_ws.Activate
        MsgBox ("Macro ended early. Please check the following column header names: " & MissingColNames)
    End If
    
    Application.DisplayStatusBar = True
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic

End Sub
